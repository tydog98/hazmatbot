import discord
import re
from discord.utils import get
from discord.ext import commands

TOKEN = ''

client = discord.Client()

reg = re.compile(r'([aA4]+([-*!_ ]+)?[nN]+([-*!_ ]+)?[iI1]+([-*!_ ]+)?[mM]+([-*!_ ]+)?[eE3]+)')

@client.event
async def on_message(message):
    quarantine = discord.utils.get(message.server.roles,name='Quarantine')
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    mo = reg.search(message.content)

    if mo.group() in message.content:
        msg = 'uh oh'.format(message)
        await client.send_message(message.channel, msg)
        await client.add_roles(message.author, quarantine)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)
